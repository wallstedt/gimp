#!/bin/bash
geany -i \
geany.sh \
SConstruct \
../lib/SConstruct \
../lib/pullinEB.h \
../lib/fit.cpp \
../lib/util.h \
../lib/tensor3.h \
../lib/managedArray.h \
../lib/shapePre.h \
../lib/main.h \
../lib/timeInt.h \
../lib/explicit.cpp \
../lib/constit.h \
../lib/constit.cpp \
../lib/shapeSC.cpp \
../lib/shapeFunc.cpp \
../lib/io.h \
../lib/main.cpp \
../lib/main.py \
../lib/license.txt \
../regress.py \
../timings.py \
../README.txt \
run.cpp \
run.py \
&
